<?php


namespace app\models;

use Yii;
use yii\base\Model;

class OrderForm extends Model
{
    const SELF_NAME = 'order-form';
    const MEALS = 'meals';
    const ORDER_PRICE = 'order_price';
    const CLIENT_EMAIL = 'client_email';
    const CLIENT_NAME = 'client_name';
    const CLIENT_PHONE = 'client_phone';

    const REQUIRED_PARAMS = [
        self::MEALS,
        self::CLIENT_EMAIL,
        self::CLIENT_NAME,
        self::ORDER_PRICE
    ];

    protected const REQUIRED_VALIDATOR = 'required';
    protected const EMAIL_VALIDATOR = 'email';
    protected const STRING_VALIDATOR = 'string';

    public array $meals;
    public float $order_price;
    public string $client_name;
    public string $client_email;
    public string $client_phone;

    /**
     * @return array
     */
    function rules(): array
    {
        return [
            [self::REQUIRED_PARAMS, self::REQUIRED_VALIDATOR],
            [self::CLIENT_EMAIL, self::EMAIL_VALIDATOR],
            [self::CLIENT_NAME, self::STRING_VALIDATOR],
            [self::CLIENT_PHONE, fn($attribute, $params, $validators) => $this->validateUserPhone($attribute, $params, $validators)],
            // todo: check why null is passed to validator
//            [self::ORDER_PRICE, fn($attribute, $params, $validators) => $this->validateDeclaredPrice($attribute, $params, $validators)]
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    protected function validateUserPhone($attribute, $params, $validator = null): void
    {

        if (!preg_match('/^[+]?[0-9]*$/', str_replace(' ', '', $params))) {
            $this->addError($attribute, 'Phone number can only have + at begining and only numbers moreover.');
        }
    }

    /**
     * @param string $sendTo
     */
    function sendOrderToKitchen(string $sendTo): void
    {
        $this->sendMessage(
            $this->client_email,
            $sendTo,
            $this->messageSubject(),
            implode('\n', [
                "Klient $this->client_name tel: $this->client_phone email: $this->client_email ",
                'Zamawia:',
                ...$this->meals
            ])
        );
    }

    /**
     * Ada[ter method
     *
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $textBody
     */
    protected function sendMessage(string $from, string $to, string $subject, string $textBody): void
    {
        Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setTextBody($textBody)
            ->send();
    }

    /**
     * @return string
     */
    protected function messageSubject(): string
    {
        return "Zamówienie z " . date("Y-m-d h:i:s");
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validators
     */
    protected function validateDeclaredPrice($attribute, $params, $validators = null): void
    {
        switch (true) {
            case !is_float($params) && !is_int($params):
                $this->addError($attribute, "Price must have form 0.00 or 0");
                break;
            case $params <= 0:
                $this->addError($attribute, 'Price must be grather than 0');
        }
    }
}