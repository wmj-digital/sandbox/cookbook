<?php


namespace app\models;


use yii\db\ActiveRecord;

class MealsCategories extends ActiveRecord
{
    const NAME = 'name';

    /**
     * @return string[]
     */
    static function getCategoriesList():array{

        return self::find()->select([self::NAME])->column();
    }
}