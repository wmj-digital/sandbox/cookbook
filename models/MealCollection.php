<?php


namespace app\models;


use Iterator;
use yii\base\Model;

class MealCollection implements Iterator
{
    protected array $mealsCategoriesList;

    /**
     * MealCollection constructor.
     *
     */
    function __construct()
    {

        $this->loadCategories();
    }

    /**
     *
     */
    protected function loadCategories():void{
        $this->mealsCategoriesList = MealsCategories::getCategoriesList();
    }

    /**
     * @return MealsMenu[]
     */
    public function current(): array
    {
        return $this->findMealsForCategory($this->key());
    }

    /**
     * Adapter method
     *
     * @param string $category
     *
     * @return MealsMenu[]
     */
    protected function findMealsForCategory(string $category): array
    {
        $condition = MealsMenu::CATEGORY . "='$category'";

        return MealsMenu::find()->where($condition)->all();
    }

    /**
     * @return string
     */
    function key(): string
    {
        return current($this->mealsCategoriesList);
    }

    /**
     * @inheritDoc
     */
    function next(): void
    {
        next($this->mealsCategoriesList);
    }

    /**
     * @return bool
     */
    function valid(): bool
    {
        return key($this->mealsCategoriesList) !== null;
    }

    /**
     * @inheritDoc
     */
    function rewind(): void
    {
        reset($this->mealsCategoriesList);
    }
}