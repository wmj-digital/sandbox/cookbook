<?php


namespace app\models;


use yii\db\ActiveRecord;

class MealsMenu extends ActiveRecord
{
    const ID = 'id';
    const CATEGORY = 'category';
    const NAME = 'name';
    const INGREDIENTS = 'ingredients';
    const PRICE = 'price';
    const ALWAYS_AVAILABLE = 'always_available';
}