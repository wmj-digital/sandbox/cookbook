<?php


interface MealsCategories
{
    const TABLE_NAME = 'meals_categories';

    const C_ID = 'id';
    const C_NAME = 'name';

    const IDX_CATEGORY_NAME = 'idx-category-name';
}