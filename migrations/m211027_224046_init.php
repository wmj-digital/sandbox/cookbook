<?php

use yii\db\Migration;
use yii\db\Schema;

require __DIR__ . "/MealsMenu.php";
require __DIR__ . "/MealsCategories.php";

/**
 * Class m211027_224046_init
 */
class m211027_224046_init extends Migration
{
    protected const NULL = ' NULL';
    protected const NOT_NULL = ' NOT NULL';
    protected const SET_NULL = ' SET NULL';
    protected const CASCADE = ' CASCADE';
    protected const DEFAULT_TRUE = ' DEFAULT TRUE';
    protected const DEFAULT_FALSE = ' DEFAULT FALSE';

    const T_MEALS_CATEGORIES = 'meals_categories';
    const T_MEALS_MENU = 'meals_menu';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(MealsCategories::TABLE_NAME, [
            MealsCategories::C_ID => $this->primaryKey(),
            MealsCategories::C_NAME => $this->string()
        ]);

        $this->createIndex(
            MealsCategories::IDX_CATEGORY_NAME,
            MealsCategories::TABLE_NAME,
            MealsCategories::C_NAME
        );

        $this->createTable(MealsMenu::TABLE_NAME, [
            MealsMenu::C_ID => Schema::TYPE_PK,
            MealsMenu::C_CATEGORY => Schema::TYPE_STRING . self::NULL,
            MealsMenu::C_NAME => Schema::TYPE_STRING . self::NULL,
            MealsMenu::C_INGREDIENTS => $this->string() . self::NULL,
            MealsMenu::C_PRICE => $this->float(2) . self::NULL,
            MealsMenu::C_ALWAYS_AVAILABLE => $this->boolean() . self::DEFAULT_TRUE,
            $this->foreignKeySyntax(
                MealsMenu::C_CATEGORY,
                MealsCategories::TABLE_NAME,
                MealsCategories::C_NAME,
                self::SET_NULL,
                self::CASCADE)
        ]);

        $this->batchInsert(MealsCategories::TABLE_NAME,
            [MealsCategories::C_NAME],
            [
                ['Zupy'],
                ['Dania ze schabu'],
                ['Dania z kurczaka'],
                ['Dania z ryb'],
                ['Zestawy obiadowe'],
                ['Pozostałe']
            ]
        );

        $this->batchInsert(MealsMenu::TABLE_NAME,
            [MealsMenu::C_CATEGORY, MealsMenu::C_NAME, MealsMenu::C_INGREDIENTS, MealsMenu::C_PRICE, MealsMenu::C_ALWAYS_AVAILABLE],
            [
                ['Dania ze schabu', 'Kotlet schabowy', '', 18, true],
                ['Dania ze schabu', 'Schab marynowany w czosnku', '', 18.50, true],
                ['Dania ze schabu', 'Schab grillowany z cebulką', '', 18.5, true],
                ['Dania ze schabu', 'Schab panierowany', 'faszerowany pieczarkami', 18.5, true],
                ['Dania ze schabu', 'Schab panierowany', 'faszerowany szynką i serem żółtym', 18.5, true],

                ['Dania z kurczaka', 'Filet z kurczaka panierowany', '', 18, true],
                ['Dania z kurczaka', 'Kotlet de volay z masłem', 'z masłem', 18.5, true],
                ['Dania z kurczaka', 'Kotlet de volay', 'z serem żółtym', 18.5, true],
                ['Dania z kurczaka', 'Polędwiczki drobiowe w chrupiącej panierce', '', 18.5, true],
                ['Dania z kurczaka', 'Filet z kurczaka po góralsku z oscypkiem i cebulką', '', 18.5, true],

                ['Dania z ryb', 'Filet z morszczuka panierowany', '', 18.5, true],

                ['Pozostałe', 'Ketchup', '', 2.5, true],
                ['Pozostałe', 'Sos czosnkowy', '', 2.5, true],
            ]
        );
    }

    protected function foreignKeySyntax(string $column, string $refTable, string $refColumn, string $onDelete, string $onUpdate):string{
        return "FOREIGN KEY ($column) REFERENCES $refColumn($refColumn) ON UPDATE $onUpdate ON DELETE $onDelete";
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211027_224046_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211027_224046_init cannot be reverted.\n";

        return false;
    }
    */
}
