<?php


interface MealsMenu
{
    const TABLE_NAME = 'meals_menu';

    const C_ID = 'id';
    const C_CATEGORY = 'category';
    const C_NAME = 'name';
    const C_INGREDIENTS = 'ingredients';
    const C_PRICE = 'price';
    const C_ALWAYS_AVAILABLE = 'always_available';

    const FK_CATEGORY_NAME = 'fk-category-name';
}