<?php

namespace tests\unit\models;

interface Index_OrderForm
{
    const M_RULES = 'rules';
    const M_VALIDATE_USER_PHONE = 'validateUserPhone';
    const M_VALIDATE_CLIENT_NAME = 'validateClientName';
    const M_VALIDATE_DECLARED_PRICE = 'validateDeclaredPrice';
    const M_SEND_ORDER_TO_KITCHEN = 'sendOrderToKitchen';
    const M_MESSAGE_SUBJECT = 'messageSubject';

    /**
     * Inherited from Model class
     */
    const M_ADD_ERROR = 'addError';
}