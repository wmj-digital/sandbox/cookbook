<?php


namespace tests\unit\models;

require "Index_OrderForm.php";

use AccessibleMethod;
use app\models\OrderForm;
use Generator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class OrderFormTest extends TestCase
{
    /**
     * @var OrderForm | MockObject
     */
    protected MockObject $orderForm;

    function setUp(): void
    {
        $this->orderForm = $this->getMockBuilder( OrderForm::class)->onlyMethods([Index_OrderForm::M_ADD_ERROR])->getMock();
    }

    /**
     * @param $invalidPhoneNumber
     *
     * @dataProvider provideInvalidPhoneNumbers
     * @throws ReflectionException
     */
    function test_add_error_when_phone_number_is_invalid(string $invalidPhoneNumber):void{
        $m_validateClientPhone = new AccessibleMethod($this->orderForm ,Index_OrderForm::M_VALIDATE_USER_PHONE);

        $this->orderForm
            ->expects($this->atLeastOnce())
            ->method(Index_OrderForm::M_ADD_ERROR);

        $m_validateClientPhone->call('' ,$invalidPhoneNumber, '');
    }

    /**
     * @return Generator
     */
    function provideInvalidPhoneNumbers(): Generator{
        yield ['stanisław'];
        yield ['phone@email.local'];
    }

    /**
     * @param string $validPhoneNumber
     *
     * @throws ReflectionException
     * @dataProvider provideValidPhoneNumbers
     */
    function test_not_add_error_when_phone_number_is_valid(string $validPhoneNumber):void{
        $m_validateClientPhone = new AccessibleMethod($this->orderForm ,Index_OrderForm::M_VALIDATE_USER_PHONE);

        $this->orderForm
            ->expects($this->never())
            ->method(Index_OrderForm::M_ADD_ERROR);

        $m_validateClientPhone->call('' ,$validPhoneNumber, '');
    }

    /**
     * @return Generator
     */
    function provideValidPhoneNumbers():Generator{
        yield ['0700500800'];
        yield ['0 700 500 800'];
        yield ['+48123456789'];
        yield ['+48 123 456 789'];
    }

    /**
     * @param $invlidPrice
     *
     * @throws ReflectionException
     * @dataProvider provideInvalidPrice
     */
    function test_add_error_when_declared_price_is_invalid($invlidPrice):void{
        $m_validateClientPhone = new AccessibleMethod($this->orderForm ,Index_OrderForm::M_VALIDATE_DECLARED_PRICE);

        $this->orderForm
            ->expects($this->atLeastOnce())
            ->method(Index_OrderForm::M_ADD_ERROR);

        $m_validateClientPhone->call('' ,$invlidPrice, '');
    }

    function provideInvalidPrice():Generator{
        yield ['My name is Stanley'];
        yield ['3,14'];
        yield ['-5'];
        yield [[]];
        yield [-5];
        yield [0];
        yield [-43,21];
    }

    /**
     * @param $validPrice
     *
     * @throws ReflectionException
     * @dataProvider provideValidPrice
     */
    function test_not_add_error_when_declared_price_is_valid($validPrice):void{
        $m_validateClientPhone = new AccessibleMethod($this->orderForm ,Index_OrderForm::M_VALIDATE_DECLARED_PRICE);

        $this->orderForm
            ->expects($this->never())
            ->method(Index_OrderForm::M_ADD_ERROR);

        $m_validateClientPhone->call('' ,$validPrice, '');
    }

    /**
     * @return Generator
     */
    function provideValidPrice():Generator{
        yield [53.20];
        yield [1000];
    }
}