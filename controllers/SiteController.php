<?php

namespace app\controllers;

use app\models\OrderForm;
use cyneek\yii2\blade\BladeBehavior;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'blade' => [
                'class' => BladeBehavior::class
            ]
        ];
    }

    /**
     * @param $action
     *
     * @return bool
     * @throws BadRequestHttpException
     */
    function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('meals_card.blade');
    }

    /**
     * @return string
     */
    function actionOrderConfirm()
    {
        $payload = $this->getPostPayload();

        if (empty($payload)) return $this->goHome()->send();
        $orderForm = $this->readOrderForm($payload);

        if(!$orderForm->validate()){
            //todo: create view with error display.
            return $this->goHome()->send();
        }

        $orderForm->sendOrderToKitchen('kitchen@locla.host');
        return $this->render('order-confirm.blade', ['model' => $orderForm]);

    }

    /**
     * @param array $payload
     *
     * @return OrderForm
     */
    protected function readOrderForm(array $payload):OrderForm{

        $orderForm = new OrderForm();
        $orderForm->offsetSet(OrderForm::MEALS, $payload[OrderForm::MEALS]);
        $orderForm->order_price = (float) $payload[OrderForm::ORDER_PRICE];
        $orderForm->client_name = $payload[OrderForm::CLIENT_NAME];
        $orderForm->client_phone = $payload[OrderForm::CLIENT_PHONE];
        $orderForm->client_email = $payload[OrderForm::CLIENT_EMAIL];

        return $orderForm;
    }

    /**
     * @return array
     */
    protected function getPostPayload(): array
    {
        return Yii::$app->request->post();
    }
}
